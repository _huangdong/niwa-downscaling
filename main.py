import os
from datetime import datetime
from typing import Optional, Sequence, List, Dict

import torch
import torch.nn as nn
import torch.utils.data
import torchvision.transforms as transforms
import numpy as np
from tap import Tap
from efficientnet_pytorch import EfficientNet
import PIL
from tap.tap import TapType
from torch.utils.tensorboard import SummaryWriter
import pandas as pd

from dataset.niwa import NiwaDS
from niwa.climate import Climate
from niwa.climate_obs import ClimateObservational
from loss.smooth_loss import SmoothLoss, GradientLoss
from writer import MySummaryWriter
from net.trl import EfficientNetTrl
from net.efficientnet_lstm import EfficientNetLSTM
from net.batchnorm_bypass import BatchNorm2dBypass

writer: MySummaryWriter


class Nop:
    def nop(self, *args, **kwargs): pass

    def __getattr__(self, item): return self.nop


class ArgumentParser(Tap):
    workers: int = 4  # number of data loading workers (default: 4)
    epochs: int = 60  # number of total epochs to run
    start_epoch: int = 0  # manual epoch number (useful on restarts)
    batch_size: int = 10
    dtype: str = 'torch.float'
    gpu: int = None  # GPU id to use
    stratified: bool = False  # Use a stratified sampler in training
    n_classes: int = 3  # Specify the number of classes.
    arch: str = 'efficientnet-b0'  # model architecture (default: efficientnet-b0)
    lr: float = 0.01  # initial learning rate
    lr_interval: int = 5  # Interval to adjust learning rate.
    momentum: float = 0.9  # momentum
    weight_decay: float = 1e-4  # weight decay (default: 1e-4)
    no_tensorboard: bool = False  # Disable tensorboard writing.
    tb_tag: str = ''  # Extra tag for tensorboard directory
    tb_prefix: str = ''  # Prefix for tensorboard directory
    niwa_gcm: str = 'JMA'  # GCM data to use
    niwa_climate: str = 'RAIN'  # climate to estimate
    niwa_region: str = 'NNI'  # one of the six regions in New Zealand
    regression_vcsn: bool = False  # Run regression for VCSN data.
    gcm_root: str = 'ECMWF'  # Root directory of the GCM data
    vcsn_root: str = 'VCSN'  # Root directory of the VCSN data
    ersst_root: str = 'ERSST'  # Root directory of the ERSST data
    save_checkpoint: bool = False  # Save model checkpoint during training
    disable_data_parallel: bool = False  # Disable the use of DataParallel
    smooth_lambda: float = 1e-6  # Coefficient of smooth loss
    smooth_type: str = 'dm'  # Type of smooth loss
    climate_standardization: bool = False  # Perform standardization for climate data.
    vcsn_grad: bool = False  # Use 2D gradients in VCSN as additional output attributes.
    optim: str = 'sgd'  # The optimizer to use. "sgd" or "adam"
    regression_scalar: str = ''  # List of indices for scalar regression
    target_segments: int = 0  # Reduce the regression target to means of segments.
    gcm_3ms: bool = False  # Stacking 3 months of GCM images into channels.
    sst_rain: bool = False  # Use ERSST+VCSN for rain prediction.
    input_channels: str = '3,3,3'  # Input channels (ECMWF, SST, VCSN)
    sst_raw: bool = False  # Use the RAW ERSST data
    trl: bool = False  # Use the Tensor-Regression-Layer
    rnn: bool = False  # Use the RNN layer
    trl_ranks: str = '10,3,3'  # Ranks of TRL
    freeze_bn_epoch: int = -1  # Freeze BN layers (training=False) after the defined epochs. -1 means never.
    reset_lr_at_bnfreeze: bool = False  # Reset the learning rate when BN is frozen.
    bn_training_in_eval: bool = False  # Set training=True for all BN layers in eval() mode.
    bypass_bn: bool = False  # Bypass all the BatchNorm layers.
    print_max_gradient: bool = False  # Print the maximum gradient in training
    init_params: bool = False  # Initialize the model parameters randomly
    variable: str = 'rain'  # Target climate variable
    rhv_mask: str = '48,1,0.2'
    rnn_size: str = '100x1'  # RNN's (hidden size) x (number of hidden layers)
    val_folds: int = 10
    report_vcsn_acc: bool = False  # Also report accuracy for VCSN regression.
    ecmwf_members: str = 'all'  # ECMWF members to use
    write_pred: bool = False  # write predictions to csv

    def __init__(self, *args, underscores_to_dashes: bool = False, explicit_bool: bool = False, **kwargs):
        super().__init__(*args, underscores_to_dashes=underscores_to_dashes, explicit_bool=explicit_bool, **kwargs)
        self.data_class = ClimateObservational

        self.regression_scalar_list = []
        self.input_channels_dict = {}
        self.n_total_input_chan = 6
        self.rhv_mask_tuple = ()
        self.rnn_size_dict = {}
        self.loss_clamp_max = torch.finfo().max

    def add_arguments(self) -> None:
        super().add_arguments()
        self.add_argument('--betas', type=tuple, default=(0.9, 0.999),
                          required=False,
                          help='The betas argument for Adam optimizer.')

    def parse_args(self, args: Optional[Sequence[str]] = None, known_only: bool = False) -> TapType:
        super().parse_args(args, known_only)

        # Argument checking
        if self.regression_vcsn:
            assert self.regression_scalar == '' or not self.vcsn_grad, \
                '--regression_scalar can not be used together with "--vcsn_grad".'
        else:
            assert self.regression_scalar == '' and not self.vcsn_grad, \
                '"--regression_scalar" or "--vcsn_grad" is only available with "--regression_vcsn".'

        self.regression_scalar_list = [] if self.regression_scalar == '' else \
            list(map(lambda x: int(x), filter(lambda _: _, self.regression_scalar.split(','))))

        self.input_channels_dict = {k: v for k, v in zip(('ECMWF', 'ERSST', 'VCSN'),
                                                         map(int, self.input_channels.split(',')))}
        self.n_total_input_chan = sum(self.input_channels_dict.values())

        self.rhv_mask_tuple = tuple(map(float, self.rhv_mask.split(',')))

        self.rnn_size_dict = {k: v for k, v in zip(('hidden_size', 'rnn_depth'),
                                                   map(int, self.rnn_size.split('x')))}
        return self


def stratified_weights(dataset, n_classes) -> list:
    count = [0] * n_classes
    for val in dataset:
        count[val[1]] += 1

    weight_per_class = [0.] * n_classes
    N = float(sum(count))
    for val in range(n_classes):
        weight_per_class[val] = N / float(count[val])
    weights = [0] * len(dataset)
    for idx, val in enumerate(dataset):
        weights[idx] = weight_per_class[val[1]]

    return weights


def setup_dataset(args: ArgumentParser, **kwargs):
    image_size = EfficientNet.get_image_size(args.arch)

    train_transforms = transforms.Compose([
        # transforms.RandomResizedCrop(image_size),
        # transforms.RandomHorizontalFlip(),
        transforms.Resize(image_size, interpolation=PIL.Image.BICUBIC),
        transforms.ToTensor(),
    ])

    val_transforms = transforms.Compose([
        transforms.Resize(image_size, interpolation=PIL.Image.BICUBIC),
        transforms.CenterCrop(image_size),
        transforms.ToTensor(),
    ])
    args.image_size = image_size
    print('Using image size', image_size)

    kwargs['sst_raw'] = args.sst_raw
    kwargs['var'] = args.variable
    kwargs['channels_dict'] = args.input_channels_dict
    kwargs['ecmwf_members'] = args.ecmwf_members

    if args.regression_vcsn:
        train_dataset = args.data_class(args.gcm_root, args.vcsn_root, mask_root=None,
                                        img_transform=train_transforms, mask_transform=None,
                                        standardize=args.climate_standardization,
                                        vcsn_grad=args.vcsn_grad,
                                        target_scalars=args.regression_scalar_list,
                                        target_segments=args.target_segments,
                                        gcm_3ms=args.gcm_3ms,
                                        rhv_mask=args.rhv_mask_tuple,
                                        ersst_root=args.ersst_root,
                                        split='train', **kwargs)
        test_dataset = args.data_class(args.gcm_root, args.vcsn_root, mask_root=None,
                                      img_transform=val_transforms, mask_transform=None,
                                      standardize=args.climate_standardization,
                                      vcsn_grad=args.vcsn_grad,
                                      target_scalars=args.regression_scalar_list,
                                      target_segments=args.target_segments,
                                      gcm_3ms=args.gcm_3ms,
                                      rhv_mask=args.rhv_mask_tuple,
                                      ersst_root=args.ersst_root,
                                      split='test', **kwargs)
        args.loss_clamp_max = 10 if args.climate_standardization else \
            10 * (train_dataset.vcsn_flatten_std ** 2)
    else:
        train_dataset = args.data_class(args.gcm_root, args.vcsn_root, mask_root=None,
                                        region=args.niwa_region,
                                        output_class=True,
                                        img_transform=train_transforms, mask_transform=None,
                                        standardize=args.climate_standardization,
                                        vcsn_grad=args.vcsn_grad,
                                        target_scalars=args.regression_scalar_list,
                                        target_segments=args.target_segments,
                                        gcm_3ms=args.gcm_3ms,
                                        rhv_mask=args.rhv_mask_tuple,
                                        ersst_root=args.ersst_root,
                                        split='train', **kwargs)
        test_dataset = args.data_class(args.gcm_root, args.vcsn_root, mask_root=None,
                                       region=args.niwa_region,
                                       output_class=True,
                                      img_transform=val_transforms, mask_transform=None,
                                      standardize=args.climate_standardization,
                                      vcsn_grad=args.vcsn_grad,
                                      target_scalars=args.regression_scalar_list,
                                      target_segments=args.target_segments,
                                      gcm_3ms=args.gcm_3ms,
                                      rhv_mask=args.rhv_mask_tuple,
                                      ersst_root=args.ersst_root,
                                      split='test', **kwargs)

    if args.stratified:
        weights = stratified_weights(train_dataset, len(train_dataset.classes))
        weights = torch.tensor(weights, dtype=args.dtype)
        train_sampler = torch.utils.data.sampler.WeightedRandomSampler(weights, len(weights))
    else:
        train_sampler = None

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=(train_sampler is None),
        num_workers=args.workers, pin_memory=True, sampler=train_sampler)

    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=len(test_dataset), shuffle=False,
        num_workers=args.workers, pin_memory=True)

    return train_loader, test_loader


def setup_model(args: ArgumentParser):
    if args.regression_vcsn:
        if len(args.regression_scalar) == 0:
            n_classes = args.data_class.n_target_features
        else:
            n_classes = len(args.regression_scalar_list)
    else:
        n_classes = args.n_classes

    if args.rnn:
        model = EfficientNetLSTM(time_steps=args.input_channels_dict.values(),
                                 output_size=n_classes, **args.rnn_size_dict)
    else:
        override_params = {'num_classes': n_classes, 'in_channels': args.n_total_input_chan}
        print("=> creating model '{}'".format(args.arch))

        if args.trl:
            override_params['ranks'] = list(map(int, args.trl_ranks.split(',')))
            override_params['output_size'] = (args.batch_size, n_classes)
            override_params['batch_size'] = args.batch_size
            model = EfficientNetTrl.from_name(args.arch, **override_params)
        else:
            model = EfficientNet.from_name(args.arch, **override_params)

    if args.regression_vcsn and args.bypass_bn:
        replace_bn_layers(model)
        for n, v in model.named_modules():
            print(f'{n}: {type(v)}')

    if args.init_params:
        for n, p in model.named_parameters():
            print(f'Initializing {n} with shape {p.shape}')
            if len(p.shape) > 1:
                torch.nn.init.xavier_uniform_(p)

    # Go parallel
    if not args.disable_data_parallel:
        model = torch.nn.DataParallel(model)

    model = model.cuda()

    return model


def setup_criterion(args):
    if args.regression_vcsn:
        # criterion = nn.MSELoss().cuda(args.gpu)
        if args.smooth_type == 'grad2d':
            kwargs = {'conversion': lambda x: args.data_class.vcsn2d(x)}
        else:
            kwargs = {}
        criterion = SmoothLoss(smooth_lambda=args.smooth_lambda,
                               smooth_type=args.smooth_type, **kwargs).cuda(args.gpu)
    else:
        criterion = nn.CrossEntropyLoss().cuda(args.gpu)

    return criterion


def setup_optimizer(model, args):
    if args.optim == 'adam':
        optimizer = torch.optim.Adam(model.parameters(),
                                     lr=args.lr,
                                     betas=args.betas,
                                     weight_decay=args.weight_decay)
    else:
        optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                    momentum=args.momentum,
                                    weight_decay=args.weight_decay)
    return optimizer


def train(train_loader, model, criterion, optimizer, epoch, args):
    # switch to train mode
    model.train()
    if 0 <= args.freeze_bn_epoch < epoch:
        set_bn_training(model, False)

    if 0 <= args.freeze_bn_epoch == epoch and args.reset_lr_at_bnfreeze:
        reset_learning_rate(optimizer, args)
        print(f'Epoch {epoch}: learning rate is reset to {args.lr}')

    n_batches = len(train_loader)

    accuracies = []
    losses = []
    for i, (images, target) in enumerate(train_loader):
        images = images.cuda(args.gpu, non_blocking=True)
        target = target.cuda(args.gpu, non_blocking=True)

        # compute output
        output = model(images)

        loss = criterion(output, target)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()

        if args.print_max_gradient:
            max_gradient = ('null', 0)
            for n, p in list(filter(lambda t: t[1].grad is not None, model.named_parameters())):
                norm = p.grad.data.norm(2).item()
                if norm > max_gradient[1]:
                    max_gradient = n, norm
            print(f'max_gradient={max_gradient}')

        # for i, (name, module) in enumerate(model.module.named_children()):
        #     print(f'{i}: {name}: {torch.tensor([p.grad.mean().item() for p in module.parameters()]).mean()}')

        optimizer.step()

        if args.regression_vcsn:
            print(f'Train {epoch} {i}/{n_batches}: loss={loss:.3f}')
        else:
            acc = accuracy(output, target)
            print(f'Train {epoch} {i}/{n_batches}: accuracy={acc[0].item()}, loss={loss:.3f}')
            accuracies.append(acc)
        # writer.add_scalar('Train/Acc', acc[0], epoch * n_batches + i)
        # writer.add_scalar('Train/Loss', loss, epoch * n_batches + i)
        losses.append(loss)
    if not args.regression_vcsn:
        writer.add_scalar('Train/Acc', torch.tensor(accuracies).mean(dim=0)[0], epoch)
    writer.add_scalar('Train/Loss', torch.tensor(losses).mean(), epoch)

    if args.save_checkpoint:
        state_dict = model.state_dict() if args.disable_data_parallel else \
            model.module.state_dict()
        torch.save({
            'epoch': epoch + 1,
            'arch': args.arch,
            'state_dict': state_dict,
            'optimizer': optimizer.state_dict(),
            'args': args.as_dict(),
        }, 'model.pth')

    return accuracies, losses


def validate(test_loader, model, criterion, args, tag='Test'):
    result = {}
    # switch to evaluate mode
    model.eval()
    if args.bn_training_in_eval:
        set_bn_training(model, True)

    losses = []
    mse_losses = []
    outputs = []
    targets = []
    with torch.no_grad():
        for i, (images, target) in enumerate(test_loader):
            images = images.cuda(args.gpu, non_blocking=True)
            target = target.cuda(args.gpu, non_blocking=True)
            output = model(images)
            print(f'{tag} (mini-batch {i}): loss={criterion(output, target):.3f}')

            targets.append(target)
            outputs.append(output)

        target = torch.cat(targets, dim=0)
        output = torch.cat(outputs, dim=0)

        if args.write_pred:
            if args.regression_vcsn:
                result['output'] = output
            else:
                result['output'] = torch.argmax(output, dim=1)

        if args.regression_vcsn:
            loss = criterion(output, target)
            print(f'{tag}: loss={loss:.3f}')
            if args.vcsn_grad:
                mse_loss = nn.MSELoss()(output[:, :args.data_class.vcsn_n_land_grids],
                                        target[:, :args.data_class.vcsn_n_land_grids])
                var = torch.var(output[:, :args.data_class.vcsn_n_land_grids])
                bias = mse_loss - var
            else:
                mse_loss = nn.MSELoss()(output, target)
                var = torch.var(output)
                bias = mse_loss - var

            if args.report_vcsn_acc:
                acc = calc_regressed_accuracy(output, target, args, verbose=(tag == 'Test'))
            else:
                acc = {}

        else:
            acc = accuracy(output, target)
            print(f'{tag}: accuracy={acc[0]}')

    if args.regression_vcsn:
        result.update({'loss': loss, 'mse_loss': mse_loss, 'bias': bias,
                       'var': var, 'acc': acc})

    else:
        result.update({'acc': acc})

    return result


def calc_regressed_accuracy(output, target, args: ArgumentParser, verbose=True):
    pred_cls = [args.data_class.flatten2category(o.cpu(), i, normalized=args.climate_standardization)
                for i, o in enumerate(output)]
    target_cls = [args.data_class.flatten2category(t.cpu(), i, normalized=args.climate_standardization)
                  for i, t in enumerate(target)]
    df_pred = pd.DataFrame(pred_cls)
    df_target = pd.DataFrame(target_cls)
    acc = (df_pred == df_target).sum() / df_pred.count()
    if verbose:
        print("Category accuracy", acc)
        print("Prediction distribution:")
        print(pd.DataFrame([(df_pred == i).sum() for i in range(1, args.n_classes + 1)]))
        print("Target distribution:")
        print(pd.DataFrame([(df_target == i).sum() for i in range(1, args.n_classes + 1)]))
    acc = acc.to_dict()

    return acc

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        target = target.to(pred.device)
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


def adjust_learning_rate(opts, epoch, args):
    if epoch % args.lr_interval != 0 or epoch == 0:
        return

    for param_group in opts.param_groups:
        lr = param_group['lr']
        print(f'Current lr={lr}')
        lr *= 0.5
        print(f'New lr={lr}')
        param_group['lr'] = lr


def reset_learning_rate(opts, args):
    for param_group in opts.param_groups:
        param_group['lr'] = args.lr


def test_plots(test_loader: torch.utils.data.DataLoader,
               train_loader: torch.utils.data.DataLoader,
               model: nn.modules.Module,
               args: ArgumentParser):
    for loader, label in zip((test_loader, train_loader), ('Test', 'Train')):
        loader = torch.utils.data.DataLoader(
            loader.dataset,
            batch_size=args.batch_size, shuffle=False,
            num_workers=1, pin_memory=True)
        targets = []
        outputs = []
        with torch.no_grad():
            for i, (images, target) in enumerate(loader):
                images = images.cuda(args.gpu, non_blocking=True)
                output = model(images)

                targets.append(target)
                outputs.append(output.cpu())

        targets = torch.cat(targets, dim=0)
        outputs = torch.cat(outputs, dim=0)
        print(f'targets.shape={targets.shape}, outputs.shape={outputs.shape}')
        if args.regression_vcsn:
            if args.regression_scalar != '':
                for i, (output, target) in enumerate(zip(outputs, targets)):
                    for j, _ in enumerate(args.regression_scalar_list):
                        writer.add_scalar(f'{label}Anomalies/Out_{j}',
                                          output[j].item(), i)
                        writer.add_scalar(f'{label}Anomalies/GT_{j}',
                                          target[j].item(), i)
                        writer.add_scalar(f'{label}Anomalies/Error_{j}',
                                          output[j].item() - target[j].item(), i)


def set_bn_training(model, state=True):
    for n, v in model.named_modules():
        if isinstance(v, torch.nn.modules.batchnorm._BatchNorm):
            v.train(state)
            print(f'Set {n}.train to {state}')


def replace_bn_layers(top_module):
    bn_layers = get_modules(top_module, by_type=torch.nn.BatchNorm2d)
    for bn in bn_layers:
        parents = get_parents(top_module, bn)
        bn = BatchNorm2dBypass(num_features=bn.num_features, bypass=True)
        for p in parents:
            setattr(p['parent'], p['attr'], bn)


def get_parents(top_module: nn.Module, child: nn.Module) -> List:
    assert isinstance(child, nn.Module), f"Expecting an nn.Module object for child, got {type(child)}"
    result = []
    for n, v in top_module.named_modules():
        if v is child:
            n = n.split('.')
            result.append({
                'parent': get_modules(top_module, by_path='.'.join(n[:-1]))[0],
                'attr': n[-1]
            })

    return result


def get_modules(top_module: nn.Module, by_type=None, by_name=None,
                by_path=None) -> List[nn.Module]:
    n_queries = 0
    for q in (by_type, by_name, by_path):
        n_queries = n_queries + 1 if q is not None else n_queries
    assert n_queries == 1, 'Exact one query in "by_type", "by_name" or "by_path" must be given.'

    result = []
    for n, v in top_module.named_modules():
        if by_type and type(v) is by_type or by_name and n.split('.')[-1] == by_name or \
                by_path is not None and n == by_path:
            result.append(v)
            if by_path is not None:
                break

    result = list(set(result))  # uniquifying
    return result


def split_train_val(dataset: torch.utils.data.Dataset, args: ArgumentParser):
    data_size = len(dataset)
    dataset_indices = list(range(data_size))
    np.random.shuffle(dataset_indices)

    val_split_index = int(np.floor(data_size / args.val_folds))
    train_idx, val_idx = dataset_indices[val_split_index:], dataset_indices[:val_split_index],

    train_sampler = torch.utils.data.SubsetRandomSampler(train_idx)
    val_sampler = torch.utils.data.SubsetRandomSampler(val_idx)

    train_loader = torch.utils.data.DataLoader(
        dataset=dataset, shuffle=False, batch_size=args.batch_size,
        num_workers=args.workers, sampler=train_sampler, pin_memory=True)
    val_loader = torch.utils.data.DataLoader(
        dataset=dataset, shuffle=False, batch_size=args.batch_size,
        num_workers=args.workers, sampler=val_sampler, pin_memory=True
    )
    return train_loader, val_loader


def main_worker(gpu, ngpus_per_node, args: ArgumentParser):
    train_loader, test_loader = setup_dataset(args)
    model = setup_model(args)
    print(model.module._modules)
    criterion = setup_criterion(args)
    optimizer = setup_optimizer(model, args)

    n_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print(f'Total number of trainable parameters: {n_total_params}')

    intermediates = {'train_loss': [], 'train_acc': [], 'val_loss': [],
                     'val_acc': [], 'test_loss': [], 'test_acc': []}

    best_metric = None
    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(optimizer, epoch, args)

        train_loader, val_loader = split_train_val(train_loader.dataset, args)

        train_acc, train_loss = train(train_loader, model, criterion, optimizer, epoch, args)
        intermediates['train_loss'].append(torch.tensor(train_loss, dtype=torch.float))
        if args.regression_vcsn:
            val_result = validate(val_loader, model, criterion, args, tag='Val')
            val_loss, val_mse_loss, val_bias, val_var, val_acc = list(map(
                val_result.get, ['loss', 'mse_loss', 'bias', 'var', 'acc']))
            writer.add_scalar('Val/Loss', val_loss.mean(dim=0).clamp(
                max=args.loss_clamp_max).item(), epoch)

            test_result = validate(test_loader, model, criterion, args)
            test_loss, mse_loss, bias, var, test_acc = list(map(
                test_result.get, ['loss', 'mse_loss', 'bias', 'var', 'acc']))
            writer.add_scalar('Test/Loss', test_loss.mean(dim=0).clamp(
                max=args.loss_clamp_max).item(), epoch)
            writer.add_scalar('Test/MSELoss', mse_loss.mean().clamp(
                max=args.loss_clamp_max).item(), epoch)
            # writer.add_scalar('Test/Bias', bias.item(), epoch)
            # writer.add_scalar('Test/Variance', var.item(), epoch)
            if args.report_vcsn_acc:
                for k in test_acc:
                    writer.add_scalar(f'Test/Acc_{k}', test_acc[k], epoch)
            intermediates['test_loss'].append(test_loss)
        else:
            intermediates['train_acc'].append(torch.tensor(train_acc, dtype=torch.float))

            val_result = validate(val_loader, model, criterion, args, tag='Val')
            val_acc = val_result['acc']
            val_acc = torch.tensor(val_acc, dtype=args.dtype)
            # writer.add_scalar('Val/Acc1', val_acc.mean(dim=0)[0].item(), epoch)
            writer.add_scalar('Val/Acc1', val_acc.mean(dim=0).item(), epoch)
            intermediates['val_acc'].append(val_acc)

            test_result = validate(test_loader, model, criterion, args)
            test_acc = test_result['acc']
            test_acc = torch.tensor(test_acc, dtype=args.dtype)
            writer.add_scalar('Test/Acc1', test_acc.mean(dim=0).item(), epoch)
            intermediates['test_acc'].append(test_acc)

        if args.write_pred:
            output = test_result['output']
            for i, v in enumerate(output):
                writer.add_vector(f'Test/Output_{i}', v)
        writer.flush()

        if args.regression_vcsn:
            metric = test_loss.mean(dim=0).item()
            is_best_epoch = (best_metric is None) or (metric < best_metric)
            if is_best_epoch:
                best_metric = metric
        else:
            metric = test_acc.mean(dim=0).item()
            is_best_epoch = (best_metric is None) or (metric > best_metric)
            if is_best_epoch:
                best_metric = metric
        if is_best_epoch:
            state_dict = model.state_dict() if args.disable_data_parallel else \
                model.module.state_dict()
            torch.save({
                'epoch': epoch + 1,
                'arch': args.arch,
                'state_dict': state_dict,
                'optimizer': optimizer.state_dict(),
                'args': args.as_dict(),
            }, f'model_best-{args.tb_tag}.pth')

    return model, train_loader, test_loader, criterion, args, intermediates


def main(argv=None):
    global writer
    args = ArgumentParser(description='NIWA Downscaling').parse_args(args=argv)
    print(args)

    # Arguments check and manipulations
    args.dtype = eval(args.dtype)

    if not args.no_tensorboard:
        current_time = datetime.now().strftime('%b%d_%H-%M-%S')
        hostname = os.uname()[1]
        writer = MySummaryWriter(os.path.join(
            "runs",
            "{}{}-{}-{}".format('' if args.tb_prefix == '' else f'{args.tb_prefix}-',
                                current_time, hostname, args.tb_tag)))

        args.hash = f'{hash(str(args)):x}'
        writer.add_text('args', str(args))
    else:
        writer = Nop()

    args.data_class = ClimateObservational

    if args.bn_training_in_eval and args.freeze_bn_epoch >= 0:
        raise ValueError('Conflicting arguments: --bn_training_in_eval and --freeze_bn_epoch')

    # End of arguments' check.

    ngpus_per_node = torch.cuda.device_count()
    result = main_worker(args.gpu, ngpus_per_node, args)
    test_plots(result[2], result[1], result[0], args)
    return result


if __name__ == '__main__':
    t_start = datetime.now()
    main()
    t_end = datetime.now()
    print(f'Execution time: {t_end - t_start} ({(t_end - t_start).total_seconds():.3f} seconds).')
