#!python
from functools import reduce
import numpy as np

header = r"""
% Preamble
\documentclass[11pt]{article}

% Packages
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{float}
\usepackage{geometry}
%\usepackage{showframe} %This line can be used to clearly show the new margins

\newgeometry{vmargin={15mm}, hmargin={12mm,17mm}}   % set the margins

% Document
\begin{document}
    \title{Regression of Flattened VCSN Using Observed VCSN+ERSST Data Stacked in 6 Channels (Test Set)}
    %\date{}
    \maketitle

    \listoffigures
    \clearpage
"""

footer = r"""
\end{document}
"""

start_month = np.datetime64('2015-04', 'M')

images = []
for i in range(21):
    month = start_month + np.timedelta64(i, 'M')

    images.append(''.join([
    r"""
    \begin{figure}[t]
        \centering
        \includegraphics[width=0.98\textwidth]{""", f'images/test_{i:03}.jpg', """}
        \label{fig:""", f'{i:03}',  r"""}
        \caption{""", f'{month.astype(str)}', r"""}
    \end{figure}
    """
    ])
    )

# Insert \clearpage in every three pictures
W = 3
images[W-1::W] = ['\n'.join(i) for i in list(zip(images[W-1::W], [r'\clearpage'] * len(images[W-1::W])))]
#images = reduce(lambda x, y: x + [*y] if type(y) is tuple else x + [y], images, [])

print('\n'.join([header, *images, footer]))

