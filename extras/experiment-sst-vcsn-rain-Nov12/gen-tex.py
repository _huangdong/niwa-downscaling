#!python
from functools import reduce

header = r"""
% Preamble
\documentclass[11pt]{article}

% Packages
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{float}
\usepackage{geometry}
%\usepackage{showframe} %This line can be used to clearly show the new margins

\newgeometry{vmargin={15mm}, hmargin={12mm,17mm}}   % set the margins

% Document
\begin{document}
    \title{Regression of Flattened VCSN Using Observed VCSN+ERSST Data}
    %\date{}
    \maketitle

    %\tableofcontents
    %\clearpage
"""

footer = r"""
\end{document}
"""

images = []
for i in range(21):
    images.append(''.join([r"""
    \begin{figure}[!h]
        \centering
        \includegraphics[width=0.98\textwidth]{""", f'images/test_{i:03}.jpg', """}
        \label{fig:""", f'{i:03}',  """}
    \end{figure}
    """])
    )

# Insert \clearpage in every three pictures
W = 3
images[W-1::W] = ['\n'.join(i) for i in list(zip(images[W-1::W], [r'\clearpage'] * len(images[W-1::W])))]
#images = reduce(lambda x, y: x + [*y] if type(y) is tuple else x + [y], images, [])

print('\n'.join([header, *images, footer]))

