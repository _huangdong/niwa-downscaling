import itertools

from tap import Tap
import torch

import main as m


class ArgumentParser(Tap):
    gcm_root: str = '/Scratch/dh146/ECMWF'
    vcsn_root: str = '/Scratch/dh146/VCSN'  # Root directory of the VCSN data


def main(argv=None):
    args = ArgumentParser(description='NIWA Downscaling').parse_args(argv)

    train_argv_base = ['--gcm_root', args.gcm_root,
                       '--vcsn_root', args.vcsn_root,
                       '--regression_vcsn',
                       ]

    lr_list = (10, 1, 0.1, 0.01)
    lr_interval_list = (5, 30, 60)
    arch_list = ('efficientnet-b0', 'efficientnet-b4')
    wd_list = (5e-3, 1e-3, 1e-4, 1e-6)
    bs_list = (16, 32, 64, 128)
    

    #lr_list = (1,)
    #lr_interval_list = (30,)
    #arch_list = ('efficientnet-b0',)
    #wd_list = (1e-3,)
    #bs_list = (30,)

    n_test_rounds = 5
    n_tb_colors = 7

    results = []
    for i, (arch, wd, lr_interval, lr, bs) in enumerate(itertools.product(
            arch_list, wd_list, lr_interval_list, lr_list, bs_list)):
        params = {'--arch': arch,
                  '--weight_decay': wd,
                  '--lr_interval': lr_interval,
                  '--lr': lr,
                  '--batch_size': bs,
                  '--epochs': min(lr_interval * 6, 120),
                  }

        for test_round in range(5):
            tb_prefix = int(i / n_tb_colors) * (n_test_rounds * n_tb_colors)\
                        + test_round * n_tb_colors + i % n_tb_colors
            params = {**params,
                      '--tb_prefix': f'{tb_prefix:04}',
                      '--tb_tag': f'HP{i:04}-T{test_round:02}',
                      }
            print(params)
            train_argv = [*train_argv_base,
                          *[str(j) for i in params.items() for j in i]
                          ]
            print(train_argv)
            try:
                model, train_loader, val_loader, criterion, train_args, intermediates = m.main(train_argv)
                print(intermediates)
                train_loss_epochs = torch.stack(intermediates['train_loss'], dim=0).mean(dim=1)
                val_loss_epochs = torch.stack(intermediates['val_loss'], dim=0).mean(dim=1)
                results.append({**params,
                                'train_loss_best': train_loss_epochs.min().item(),
                                'val_loss_best': val_loss_epochs.min().item(),
                                'train_loss_mean': train_loss_epochs.mean().item(),
                                'val_loss_mean': val_loss_epochs.mean().item(),
                                'train_loss_std': train_loss_epochs.std().item(),
                                'val_loss_std': val_loss_epochs.std().item(),
                                })
            except RuntimeError as e:
                if 'CUDA out of memory' in e.args[0]:
                    results.append({**params, 'Error': 'CUDA OOM'})
                else:
                    raise e

            print(results)


if __name__ == '__main__':
    main()
