import torch
from torch import Tensor


class BatchNorm2dBypass(torch.nn.BatchNorm2d):
    def __init__(self, num_features, eps=1e-5, momentum=0.1, affine=True,
                 track_running_stats=True, bypass=True):
        super().__init__(num_features, eps, momentum, affine, track_running_stats)
        self.bypass = bypass

    def forward(self, input: Tensor) -> Tensor:
        return input
        # if self.bypass:
        #     return input
        # else:
        #     return super().forward(input)
