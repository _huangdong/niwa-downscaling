import torch
from efficientnet_pytorch import EfficientNet


class EfficientNetLSTM(torch.nn.Module):
    cnn_arch = 'efficientnet-b0'
    n_cnn_feat = 1280

    def __init__(self, time_steps=(3, 3), hidden_size=100, output_size=6,
                 rnn_depth=1, **kwargs):
        super().__init__()

        self.time_steps = time_steps = list(filter(None, time_steps))
        self.hidden_size = hidden_size
        self.rnn_depth = rnn_depth
        self.output_size = output_size
        self.image_size = EfficientNet.get_image_size(self.cnn_arch)

        for i, time_step in enumerate(time_steps):
            cnn = EfficientNet.from_name(self.cnn_arch, in_channels=1,
                                         include_top=False)
            rnn = torch.nn.LSTM(input_size=self.n_cnn_feat, hidden_size=hidden_size, num_layers=rnn_depth)
            setattr(self, f'_cnn{i}', cnn)
            setattr(self, f'_rnn{i}', rnn)

        self._fc = torch.nn.Linear(hidden_size * len(time_steps), output_size)

    def forward(self, x):
        assert isinstance(x, torch.Tensor) and x.shape[1:] == (
            sum(self.time_steps), self.image_size, self.image_size)

        ch_idx = 0
        rnn_outputs = []
        for i, time_step in enumerate(self.time_steps):
            cnn = getattr(self, f'_cnn{i}')
            rnn = getattr(self, f'_rnn{i}')
            assert None not in (cnn, rnn)

            rnn.flatten_parameters()
            # Apply the CNN for each channels separately
            features = [cnn(x[:, j:j+1, ...]).flatten(start_dim=1) for j in range(ch_idx, ch_idx + time_step)]
            features = torch.stack(features, dim=0)  # Now dim 0 is the seq#
            ch_idx += time_step

            h0 = torch.randn(self.rnn_depth, features.shape[1], self.hidden_size).to(x.device)
            c0 = torch.randn(self.rnn_depth, features.shape[1], self.hidden_size).to(x.device)
            output, (hn, cn) = rnn(features, (h0, c0))  # output shape: #seq, #batch, #hidden
            rnn_outputs.append(output[-1])

        rnn_outputs = torch.cat(rnn_outputs, dim=1)  # rnn_outputs shape: #batch, #cat_hiddens
        result = self._fc(rnn_outputs)

        return result
