import torch.nn as nn
import numpy as np
import tensorly as tl
from tensorly.tenalg import inner
from tensorly.random import check_random_state
import efficientnet_pytorch


tl.set_backend('pytorch')


class TRL(nn.Module):
    """
    Reference: https://github.com/JeanKossaifi/IEEE_companion_notebooks/blob/master/tensor_regression_layer_pytorch.ipynb
    """
    def __init__(self, input_size, ranks, output_size, verbose=1, **kwargs):
        super(TRL, self).__init__()
        self.ranks = list(ranks)
        self.verbose = verbose

        if isinstance(output_size, int):
            self.input_size = [input_size]
        else:
            self.input_size = list(input_size)

        if isinstance(output_size, int):
            self.output_size = [output_size]
        else:
            self.output_size = list(output_size)

        self.n_outputs = int(np.prod(output_size[1:]))

        # Core of the regression tensor weights
        print(f'self.ranks={self.ranks}')
        self.core = nn.Parameter(tl.zeros(self.ranks), requires_grad=True)
        self.bias = nn.Parameter(tl.zeros(1), requires_grad=True)
        weight_size = list(self.input_size[1:]) + list(self.output_size[1:])
        print(f'TRL: Number of parameters: core: {self.core.numel()}, bias: {self.bias.numel()}')

        # Add and register the factors
        self.factors = []
        for index, (in_size, rank) in enumerate(zip(weight_size, ranks)):
            self.factors.append(nn.Parameter(tl.zeros((in_size, rank)), requires_grad=True))
            self.register_parameter('factor_{}'.format(index), self.factors[index])
            print(f'TRL: number of parameters: factor[{index}]: {self.factors[index].numel()}')

        print(f'TRL: Total parameters: {sum(p.numel() for p in self.parameters() if p.requires_grad)}')

        # FIX THIS
        self.core.data.uniform_(-0.1, 0.1)
        for f in self.factors:
            f.data.uniform_(-0.1, 0.1)

    def forward(self, x):
        regression_weights = tl.tucker_to_tensor((self.core, self.factors))
        return inner(x, regression_weights, n_modes=tl.ndim(x) - 1) + self.bias

    def penalty(self, order=2):
        penalty = tl.norm(self.core, order)
        for f in self.factors:
            penatly = penalty + tl.norm(f, order)
        return penalty


class EfficientNetTrl(efficientnet_pytorch.EfficientNet):
    def __init__(self, blocks_args=None, global_params=None,
                 ranks=None, output_size=None, batch_size=16):
        super().__init__(blocks_args, global_params)

        num_classes = global_params.num_classes
        self._trl = TRL(ranks=ranks, input_size=(batch_size, 320, 8, 8),
                        output_size=output_size)

    def forward(self, inputs):
        """EfficientNet's forward function.
           Calls extract_features to extract features, applies final linear layer, and returns logits.

        Args:
            inputs (tensor): Input tensor.

        Returns:
            Output of this model after processing.
        """
        # Convolution layers
        x = self.extract_features(inputs)
        # # Pooling and final linear layer
        # x = self._avg_pooling(x)
        # if self._global_params.include_top:
        #     x = x.flatten(start_dim=1)
        #     x = self._dropout(x)
        #     x = self._fc(x)
        x = self._trl(x)
        return x

    def extract_features(self, inputs):
        """use convolution layer to extract feature .

        Args:
            inputs (tensor): Input tensor.

        Returns:
            Output of the final convolution
            layer in the efficientnet model.
        """
        # Stem
        x = self._swish(self._bn0(self._conv_stem(inputs)))

        # Blocks
        for idx, block in enumerate(self._blocks):
            drop_connect_rate = self._global_params.drop_connect_rate
            if drop_connect_rate:
                drop_connect_rate *= float(idx) / len(self._blocks) # scale drop connect_rate
            x = block(x, drop_connect_rate=drop_connect_rate)

        # Head
        # x = self._swish(self._bn1(self._conv_head(x)))

        return x

    @classmethod
    def from_name(cls, model_name, in_channels=3, **override_params):
        """create an efficientnet model according to name.

        Args:
            model_name (str): Name for efficientnet.
            in_channels (int): Input data's channel number.
            override_params (other key word params):
                Params to override model's global_params.
                Optional key:
                    'width_coefficient', 'depth_coefficient',
                    'image_size', 'dropout_rate',
                    'num_classes', 'batch_norm_momentum',
                    'batch_norm_epsilon', 'drop_connect_rate',
                    'depth_divisor', 'min_depth'

        Returns:
            An efficientnet model.
        """
        cls._check_model_name_is_valid(model_name)

        ranks = override_params.pop('ranks')
        output_size = override_params.pop('output_size')
        batch_size = override_params.pop('batch_size')

        from efficientnet_pytorch import get_model_params
        blocks_args, global_params = get_model_params(model_name, override_params)
        model = cls(blocks_args, global_params, ranks=ranks,
                    output_size=output_size, batch_size=batch_size)
        model._change_in_channels(in_channels)
        return model
