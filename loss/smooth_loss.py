from typing import Any, Union
from functools import reduce
from collections import OrderedDict

import torch
from torch import Tensor
from torch.nn.functional import conv2d
from torch.nn.modules import Module
from torch.nn.modules.loss import MSELoss


class DiffMatrixLoss(Module):
    r"""
    Compute a difference matrix for the input and the target respectively, and
    output the mean of element-wise error of the two matrices.

    For the :math:`n`-dimensional input or target :math:`y`, the difference matrix
    :math:`D` is computed as:

    .. math::
        D_{i, j} = y_i - y_j

    """

    def __init__(self):
        super(DiffMatrixLoss, self).__init__()

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        assert len(input.shape) == 2 and len(target.shape) == 2, \
            f'Expect both input and target are 2-D tensors: (n_instances, n_features).'

        return MSELoss()(torch.mean(input, dim=1), torch.mean(target, dim=1))


class GradientLoss(Module):
    def __init__(self, n_channels=1, conversion=None):
        super(GradientLoss, self).__init__()

        self.weight_dx = torch.tensor([[0, 0, 0],
                                       [-1, 0, 1],
                                       [0, 0, 0]], dtype=torch.float
                                      ).repeat(n_channels, 1, 1, 1)
        self.weight_dy = self.weight_dx.transpose(2, 3)

        self.n_channels = n_channels
        self.conversion = conversion

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        if self.conversion is not None:
            # input = self.conversion(input)
            target = self.conversion(target)

        # Unsqueeze the shape (minibatch, iH, hW) to (minibatch, in_channels, iH, iW)
        # given in_channels = 1.
        # input = input.unsqueeze(1) if len(input.shape) == 3 else input
        target = target.unsqueeze(1) if len(target.shape) == 3 else target

        dx = conv2d(target, self.weight_dx.to(target.device)).unsqueeze(-1)
        dy = conv2d(target, self.weight_dy.to(target.device)).unsqueeze(-1)

        dx[torch.isnan(dx) | torch.isinf(dx)] = 0
        dy[torch.isnan(dy) | torch.isinf(dy)] = 0

        grad2d = torch.cat((dx, dy), dim=-1)
        norm = torch.norm(grad2d, p='fro')

        return norm


class SmoothLoss(Module):
    def __init__(self, smooth_type: Union[str, type] = 'dm',
                 smooth_lambda: float = 1, **kwargs):
        super().__init__()

        if type(smooth_type) is str:
            smooth_type = {'dm': DiffMatrixLoss,
                           'grad2d': GradientLoss,
                           }[smooth_type]

        if smooth_type is GradientLoss:
            smooth_kwargs = {'n_channels': kwargs.pop('n_channels', 1),
                             'conversion': kwargs.pop('conversion', None)}
        else:
            smooth_kwargs = {}

        self.smooth_loss = smooth_type(**smooth_kwargs)
        self.lambda_smooth = smooth_lambda

        self.mse_loss = MSELoss(**kwargs)

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        loss = self.mse_loss(input, target)
        if self.lambda_smooth >= 1e-12:
            loss += self.lambda_smooth * self.smooth_loss(input, target)
        return loss
