import argparse
import random
from datetime import datetime
import sys
import os
import re
from typing import Union
from functools import partial

import torch
from torch.nn.functional import conv2d, interpolate
import h5py
import numpy as np
from pathlib import Path

from glob import glob
import torch.utils.data as data
import xarray as xr
import pandas as pd
from dask.diagnostics import ProgressBar

LON_RANGE = (70, 294)
LAT_RANGE = (-70, 58)
TIME_BASIS = 'seasonal'  # 'monthly' or 'seasonal'
TIME_FIRST_TRAIN = {'monthly': np.datetime64('1981-04', 'M'),
                    'seasonal': np.datetime64('1981-06', 'M')}[TIME_BASIS]
TIME_LAST_TRAIN = np.datetime64('2014-12', 'M')
TIME_FIRST_TEST = {'monthly': np.datetime64('2015-01', 'M'),
                   'seasonal': np.datetime64('2015-03', 'M')}[TIME_BASIS]
TIME_LAST_TEST = np.datetime64('2019-12', 'M')

ECMWF_STEP = {'monthly': 1, 'seasonal': 3}[TIME_BASIS]
ECMWF_SHIFT = {'monthly': 2, 'seasonal': 2}[TIME_BASIS]

VCSN_SUBDIR_DICT = {'rain': 'RAIN', 'temp': 'TMEAN'}
VCSN_DATAVAR_DICT = {'rain': 'Rain_bc', 'temp': 'Tmean_N'}

ECMWF_SUBDIR_DICT = {'rain': 'PRECIP', 'temp': 'T2M'}
ECMWF_DATAVAR_DICT = {'rain': 'precip', 'temp': 't2m'}

REGIONAL_CLIM_SUBDIR_DICT = {'rain': 'Rain_bc', 'temp': 'Tmean'}
REGIONAL_CLIM_DATAVAR_DICT = {'rain': 'Rain_bc_average', 'temp': 'Tmean_average'}


def _get_month_str(data):
    if hasattr(data, 'strftime'):
        return data.strftime('%Y-%m')
    elif isinstance(data, np.datetime64):
        return data.astype('datetime64[M]').astype(str)
    else:
        raise ValueError(f"Cannot get the month of data with type `{type(data)}': {data}")


def generate_mask(ds_outer, ds_inner, radius, scales=(1, 0.5)):
    grids = np.zeros((len(ds_outer.lat), len(ds_outer.lon), 2))
    grids[:, :, 0] = np.outer(ds_outer.lat.data, np.ones(grids.shape[1]))\
                     - ds_inner.lat.mean().data.item()
    grids[:, :, 0] *= scales[0]
    grids[:, :, 1] = np.outer(np.ones(grids.shape[0]), ds_outer.lon.data)\
                     - ds_inner.lon.mean().data.item()
    grids[:, :, 1] *= scales[1]

    distance = np.sqrt(grids[:, :, 0] ** 2 + grids[:, :, 1] ** 2)
    inner_radius = max(ds_inner.lat.max() - ds_inner.lat.min(),
                       ds_inner.lon.max() - ds_inner.lon.min())
    half_radius = radius / 2
    quarter_radius = radius / 4

    def _mask(x):
        x = (x - inner_radius/2 - half_radius) / quarter_radius
        return 0.5 * (1 - np.tanh(x))

    return np.vectorize(_mask)(distance)


class ClimateObservational(torch.utils.data.Dataset):
    vcsn_n_land_grids = 11_491
    vcsn_n_total_flattened = vcsn_n_land_grids
    n_target_features = -1
    ecmwf_cache = {}
    ersst_cache = {}
    vcsn_cache = {}
    vcsn_flatten_cache = {}
    vcsn_mean = None
    vcsn_std = None
    vcsn_mask = None
    vcsn_mask_da = None
    climato_mean = None
    df_rq = None
    ds_regional_masks = None
    ecmwf_mean = None
    ecmwf_std = None
    ersst_mean = None
    ersst_std = None
    vcsn_flatten_mean = None
    vcsn_flatten_std = None

    ecmwf_mask = None
    ersst_mask = None

    def __init__(self, gcm_root, vcsn_root, mask_root, img_transform, mask_transform,
                 ersst_root='ERSST',
                 split='train', image_size=72, n_masks=10000, regenerate=False,
                 sanity_check_area='', gt_vcsn=False, gt_gcm=False,
                 reuse_data_cache=True,
                 standardize=False,
                 vcsn_grad=False,
                 target_scalars=(),
                 target_segments=0,
                 rhv_mask=(48, 1, 0.2),
                 gcm_3ms=False,
                 sst_raw=False,
                 write_dataset=False,
                 var='rain',
                 channels_dict=None,
                 ecmwf_members='all',
                 region='NNI',
                 output_class=False,
                 ):
        super(ClimateObservational, self).__init__()

        self.image_size = image_size
        self.n_masks = n_masks

        self.img_transform = img_transform
        self.transform = img_transform
        self.mask_transform = mask_transform
        self.subset = split
        self.gcm_root = str(gcm_root)
        self.vcsn_root = str(vcsn_root)
        self.ersst_root = str(ersst_root)
        self.regenerate = regenerate
        self.write_dataset = write_dataset
        self.sc_zone = sanity_check_area
        self.new_approach = True
        self.gt_vcsn = gt_vcsn
        self.gt_gcm = gt_gcm
        self.standardize = standardize
        self.vcsn_grad = vcsn_grad
        self.target_scalars = target_scalars
        self.target_segments = target_segments
        self.gcm_3ms = gcm_3ms
        self.sst_raw = sst_raw
        self.channels_dict = channels_dict if channels_dict else {'ECMWF': 0, 'ERSST': 3, 'VCSN': 3}
        self.rhv_mask = rhv_mask

        self.region = region
        self.output_class = output_class

        if ecmwf_members == 'all' or ecmwf_members is None:
            self.ecmwf_members = ()
        else:
            self.ecmwf_members = list(map(int, ecmwf_members.split(',')))

        assert var in ('rain', 'temp')
        self.var = var

        self.maskpath = mask_root

        self.reuse_data_cache = reuse_data_cache
        self._prepare_data()

    def __getitem__(self, index):
        """
        The dataset item is organized as follow:
        input: stitching 2D ERSST (180x90) at the top and VCSN-3m (80x85), VCSN-2m,
         VCSN-1m at the bottom.
        target: flattened VCSN at current month
        :param index:
        :return:
        """
        real_index = index + max(self.channels_dict.values())
        w = 224
        h = 224

        channels = []
        for i in range(self.channels_dict['ECMWF'], 0, -1):
            c = interpolate(torch.from_numpy((self.ecmwf[real_index - i] * self.ecmwf_mask)[None, None]), (h, w))
            channels.append(c[0, ...])
        for i in range(self.channels_dict['ERSST'], 0, -1):
            c = interpolate(torch.from_numpy((self.ersst[real_index - i] * self.ersst_mask)[None, None]), (h, w))
            channels.append(c[0, ...])
        for i in range(self.channels_dict['VCSN'], 0, -1):
            c = interpolate(torch.from_numpy(self.vcsn[real_index - i][None, None]), (h, w))
            channels.append(c[0, ...])

        image = torch.cat(channels, dim=0)
        image = image.float()

        # # ERSST observation in the last month
        # img_ersst_1m = interpolate(torch.from_numpy(self.ersst[real_index - 1][None, None]), (h, w))
        # img_ersst_2m = interpolate(torch.from_numpy(self.ersst[real_index - 2][None, None]), (h, w))
        # img_ersst_3m = interpolate(torch.from_numpy(self.ersst[real_index - 3][None, None]), (h, w))
        # # print(f'{img_ersst_3m.shape=}')
        #
        # # VCSN observations in the last 3 months
        # img_vcsn_3m = interpolate(torch.from_numpy(self.vcsn[real_index - 3][None, None]), (h, w))
        # img_vcsn_2m = interpolate(torch.from_numpy(self.vcsn[real_index - 2][None, None]), (h, w))
        # img_vcsn_1m = interpolate(torch.from_numpy(self.vcsn[real_index - 1][None, None]), (h, w))
        # # print(f'{img_vcsn_1m.shape=}')
        # image = torch.cat([img_ersst_3m[0, ...], img_ersst_2m[0, ...], img_ersst_1m[0, ...],
        #                    img_vcsn_3m[0, ...], img_vcsn_2m[0, ...], img_vcsn_1m[0, ...]], dim=0)
        # print(f'{image.shape=}')
        # image = image.float()

        # Target VCSN observation in current month
        target = torch.from_numpy(self.vcsn_flatten[real_index]).float()
        if self.target_aug_func is not None:
            target = self.target_aug_func(target)

        if self.output_class:
            target = self.flatten2category(target, index, split=self.subset,
                                           normalized=self.standardize)[self.region]
            target -= 1  # 1-base to 0-base

        # if len(self.scalars) > 0:
        #     selector = np.isin(np.arange(self.vcsn_flatten.shape[-1]), self.scalars)
        #     target = torch.from_numpy(self.vcsn_flatten[real_index, selector]).float()
        # else:
        #     target = torch.from_numpy(self.vcsn_flatten[real_index]).float()

        return image, target

    def __len__(self):
        return len(self.ersst) - max(self.channels_dict.values())

    def _get_vcsn(self) -> xr.DataArray:
        subdir = VCSN_SUBDIR_DICT[self.var]
        datavar = VCSN_DATAVAR_DICT[self.var]
        with xr.open_mfdataset(glob(f'{self.vcsn_root}/{subdir}/*{TIME_BASIS}*.nc')) as ds:
            print(ds)
            vcsn = ds[datavar]
            type(self).vcsn_mask = ~np.isnan(ds.mask).compute().data
            type(self).vcsn_mask_da = ds.mask

        return vcsn

    def _get_ersst(self) -> xr.DataArray:
        if not self.sst_raw:
            ersst_files = rf'{self.ersst_root}/ERSST_{TIME_BASIS}_anomalies_*.nc'
            with xr.open_mfdataset(ersst_files) as ds:
                sst = ds['sst']
        else:
            ersst_files = [f'{self.ersst_root}/ersst.v5.{year}{month:02}.nc'
                           for year in range(1993, 2017) for month in range(1, 13)]

            with xr.open_mfdataset(ersst_files, concat_dim=['time'], combine='nested') as ds:
                sst = ds['sst'].isel({'lev': 0}, drop=True)

            # The ERSST dataset may be read from NetCDF files with different type of "time"
            # variables, e.g., `pandas._libs.tslibs.timestamps.Timestamp`,
            # `cftime._cftime.Datetime360Day` or `numpy.datetime64[ns]`, with different
            # day of month as well. We reassign a consistent datetime64[ns] time
            # aligning the 15th day of each month for the entire dataset.
            first_month = _get_month_str(sst.time.data[0])
            last_month = _get_month_str(sst.time.data[-1])
            time_series = np.arange(np.datetime64(first_month, 'M'),
                                    np.datetime64(last_month, 'M') + np.timedelta64(1, 'M'),
                                    np.timedelta64(1, 'M')
                                    ).astype('datetime64[ns]') + np.timedelta64('14', 'D')
            sst = sst.assign_coords({'time': time_series})

            for i in range(len(sst.time)):
                print(f'{sst.time.data[i]}: {type(sst.time.data[i])}')

        return sst

    def _get_ecmwf(self) -> xr.DataArray:
        subdir = ECMWF_SUBDIR_DICT[self.var]
        datavar = ECMWF_DATAVAR_DICT[self.var]

        from glob import glob
        ecmwf_files = list(sorted(filter(lambda x: re.match(
            rf'.*ECMWF_{subdir}_\d+-\d+_{TIME_BASIS}_anomalies_1981_2010_clim.nc', x
        ) is not None, glob(f'{self.gcm_root}/{subdir}/CDS_ECMWF_*.nc'))))
        with xr.open_mfdataset(ecmwf_files, concat_dim='time', parallel=True, combine='nested') as ds:
            if 'init_time' in ds:
                ds = ds.rename({'init_time': 'time'})
            ecmwf = ds[datavar]

        if len(self.ecmwf_members) == 0:
            self.ecmwf_members = list(range(len(ecmwf.member)))
        ecmwf = ecmwf.sel({'member': self.ecmwf_members})
        print(f'{ecmwf.member=}')

        print('Read ECMWF dataset:', ecmwf)
        return ecmwf

    def _get_regional_climato(self) -> xr.DataArray:
        subdir = REGIONAL_CLIM_SUBDIR_DICT[self.var]
        datavar = REGIONAL_CLIM_DATAVAR_DICT[self.var]
        climato_file = os.path.join(os.path.dirname(__file__),
                                    f'../dataset/climatology_average_Q33_Q66_{subdir}.nc')
        with xr.open_dataset(climato_file) as ds:
            type(self).climato_mean = ds[datavar].transpose(
                'month', 'lat', 'lon'
            )

        return self.climato_mean

    def _get_regional_quantities(self) -> None:
        subdir = REGIONAL_CLIM_SUBDIR_DICT[self.var]
        datavar = REGIONAL_CLIM_DATAVAR_DICT[self.var]
        quantities_file = os.path.join(os.path.dirname(__file__),
                                       f'../dataset/Climatological_quantiles_3_cat_{subdir}.csv')
        df = pd.read_csv(quantities_file, header=2, index_col=0)
        df.columns = [f'{region}_{split}' for region in ('NNI', 'ENI', 'WNI', 'NSI', 'WSI', 'ESI')
                      for split in ('Q33', 'Q66')]
        type(self).df_rq = df

    def _get_regional_masks(self) -> None:
        nc_file = 'dataset/NZ_6_regions_mask.nc'
        with xr.open_dataset(nc_file) as ds:
            type(self).ds_regional_masks = ds

    def _process_data_regression(self):
        # Read the ECMWF data
        ecmwf = self._get_ecmwf()
        ersst = self._get_ersst()
        vcsn = self._get_vcsn()
        self._get_regional_climato()
        self._get_regional_quantities()
        self._get_regional_masks()

        # Align the time between ECMWF and VCSN
        time_first = TIME_FIRST_TRAIN if self.subset == 'train' else TIME_FIRST_TEST
        time_last = TIME_LAST_TRAIN if self.subset == 'train' else TIME_LAST_TEST
        delta_step = np.timedelta64(ECMWF_STEP, 'M')
        delta_1month = np.timedelta64(1, 'M')
        vcsn = vcsn.where((time_first <= vcsn.time)
                          & ((time_last + delta_1month) > vcsn.time),
                          drop=True)
        print('VCSN time range after alignment:', vcsn.time)
        # The ECMWF data is predictive, whose timestamps are at 00:00:00 of
        # the 1st of each month. The step-1 data is to predict the climate
        # till the end of the same month.
        #
        # On the other hand, as a convention in this project, we always use
        # indices (i-1), (i-2), ... to predict target at index i.
        #
        # As a result, we would like to align the ECMWF with step s at i-1.

        # Moreover, we use integer indexing to avoid the annoying calendar
        # calculation functions.
        offset = (vcsn.time[0] - ecmwf.time[0]).data.astype(
            'timedelta64[M]').astype(int).item()
        shift = ECMWF_SHIFT
        ecmwf = ecmwf.isel({'time': slice(offset - ECMWF_STEP + shift,
                                          offset - ECMWF_STEP + shift + len(vcsn.time))})
        print('ECMWF time range after alignment:', ecmwf.time)

        offset = (vcsn.time[0] - ersst.time[0]).data.astype(
            'timedelta64[M]').astype(int).item()
        print(f'ERSST offset = {offset}')
        shift = 1 if TIME_BASIS == 'monthly' else -2
        ersst = ersst.isel({'time': slice(offset + shift,
                                          offset + shift + len(vcsn.time))})
        # ersst = ersst.where((time_first <= ersst.time)
        #                     & (time_last >= ersst.time),
        #                     drop=True)
        print('ERSST time range after alignment:', ersst.time)

        # Aggregate in the ensembles and pick only one step.
        ecmwf = ecmwf.sel({'step': ECMWF_STEP}).mean(dim='member')

        # Rearrange the dimensions
        ecmwf = ecmwf.transpose('time', 'lat', 'lon')
        ersst = ersst.transpose('time', 'lat', 'lon')
        vcsn = vcsn.transpose('time', 'lat', 'lon')

        # Flatten VCSN
        vcsn_time_length = len(vcsn.time)
        n_flatten_feat = self.vcsn_n_land_grids
        vcsn_flatten = vcsn.data.reshape(vcsn_time_length, -1)
        if hasattr(vcsn_flatten, 'compute'):
            vcsn_flatten = vcsn_flatten.compute()
        vcsn_flatten = vcsn_flatten[~np.isnan(vcsn_flatten)]
        if vcsn_flatten.shape[0] != n_flatten_feat * vcsn_time_length:
            for i, instance in enumerate(vcsn.data.reshape(vcsn_time_length, -1)):
                print(f'instance={instance}')
                index = ~np.isnan(instance)
                print(f'index={index}')
                instance = instance[index]
                print(instance)
                l = len(instance)
                l = len(instance[~np.isnan(instance)])
                if l != n_flatten_feat:
                    raise ValueError(f'VCSN at time {vcsn[i].time} has incorrect '
                                     f'number of valid grid points: '
                                     f'expected {n_flatten_feat}, got {l}.')
        vcsn_flatten = vcsn_flatten.reshape(vcsn_time_length, n_flatten_feat)

        # If `vcsn_grad` specified, append the flattened 2D gradient to the attributes.
        if self.vcsn_grad:
            # img2d.shape: (time, channel=1, lat, lon)
            img2d = torch.from_numpy(vcsn.data.compute()).unsqueeze(1)
            dx = conv2d(img2d, torch.tensor(
                [[[[0, 0, 0], [-1, 0, 1], [0, 0, 0]]]], dtype=torch.float))
            dy = conv2d(img2d, torch.tensor(
                [[[[0, -1, 0], [0, 0, 0], [0, 1, 0]]]], dtype=torch.float))
            grad2d = torch.cat((dx.unsqueeze(-1), dy.unsqueeze(-1)), dim=-1)
            grad2d = grad2d.squeeze(1)  # remove the channel dimension
            grad2d_norm = grad2d.norm(dim=-1, p='fro')
            grad2d_norm_flatten = grad2d_norm[~torch.isnan(grad2d_norm)].view(
                vcsn_time_length, -1)
            print(f'grad2d_norm_flatten.shape={grad2d_norm_flatten.shape}')

            vcsn_flatten = np.hstack((vcsn_flatten, grad2d_norm_flatten.numpy()))
            print(f'vcsn_flatten.shape={vcsn_flatten.shape}')

            type(self).vcsn_n_total_flattened = vcsn_flatten.shape[1]

        if type(self).ecmwf_mask is None:
            type(self).ecmwf_mask = generate_mask(ecmwf, vcsn,
                                                  radius=self.rhv_mask[0],
                                                  scales=(self.rhv_mask[1], self.rhv_mask[2]))
        if type(self).ersst_mask is None:
            type(self).ersst_mask = generate_mask(ersst, vcsn,
                                                  radius=self.rhv_mask[0],
                                                  scales=(self.rhv_mask[1], self.rhv_mask[2]))

        ecmwf = ecmwf.data
        if hasattr(ecmwf, 'compute'):
            ecmwf = ecmwf.compute()
            ecmwf = np.nan_to_num(ecmwf, nan=np.nanmean(ecmwf).item())

        ersst = ersst.data
        if hasattr(ersst, 'compute'):
            ersst = ersst.compute()
            ersst = np.nan_to_num(ersst, nan=np.nanmean(ersst).item())

        vcsn = vcsn.data
        if hasattr(vcsn, 'compute'):
            vcsn = vcsn.compute()
            vcsn = np.nan_to_num(vcsn, nan=np.nanmean(vcsn).item())

        if self.subset == 'train':
            type(self).vcsn_mean = vcsn.mean()
            type(self).vcsn_std = vcsn.std()

            type(self).ersst_mean = ersst.mean()
            type(self).ersst_std = ersst.std()

            type(self).ecmwf_mean = ecmwf.mean()
            type(self).ecmwf_std = ecmwf.std()

            type(self).vcsn_flatten_mean = vcsn_flatten.mean()
            type(self).vcsn_flatten_std = vcsn_flatten.std()

        # Perform a global standardization for both ERSST and VCSN data so that
        # they can be concatenated.
        assert None not in (self.ecmwf_mean, self.ecmwf_std, self.ersst_mean,
                            self.ersst_std, self.vcsn_mean, self.vcsn_std)
        ecmwf = (ecmwf - self.ecmwf_mean) / self.ecmwf_std
        ersst = (ersst - self.ersst_mean) / self.ersst_std
        vcsn = (vcsn - self.vcsn_mean) / self.vcsn_std
        if self.standardize:
            assert self.vcsn_flatten_mean is not None, 'Train set must be constructed first.'
            vcsn_flatten = (vcsn_flatten - self.vcsn_flatten_mean) / self.vcsn_flatten_std

        self.ecmwf = ecmwf
        self.ersst = ersst
        self.vcsn_flatten = vcsn_flatten
        self.vcsn = vcsn

        type(self).ecmwf_cache[self.subset] = ecmwf
        type(self).ersst_cache[self.subset] = ersst
        type(self).vcsn_flatten_cache[self.subset] = vcsn_flatten

    def _prepare_data(self):
        """
        Data preparation.

        The first step of experiments:

        Using the combination of VCSN data for New Zealand and ERA5 for the rest in the world.

        :return:
        """

        if self.reuse_data_cache and self.ersst_cache.get(self.subset) is not None \
                and self.vcsn_flatten_cache.get(self.subset) is not None:
            print('Reusing cached GCM and VCSN data.')
            self.ersst = self.ersst_cache[self.subset]
            self.ecmwf = self.ecmwf_cache[self.subset]
            self.vcsn_flatten = self.vcsn_flatten_cache[self.subset]
        else:
            self._process_data_regression()
            print('GCM and VCSN databases processed.')

        self.target_aug_func = None

        if len(self.target_scalars) > 0:
            selector = np.isin(np.arange(self.vcsn_flatten.shape[-1]), self.target_scalars)
            self.target_aug_func = lambda feat: feat[selector]
        elif self.target_segments > 0:
            n_segments = self.target_segments

            def segmented_means(feat):
                result = type(feat)(list(map(torch.mean, torch.split(feat, [
                    int(len(feat) / n_segments) if i < n_segments else (len(feat) - (n_segments - 1) * int(len(feat) / n_segments))
                    for i in range(1, n_segments+1)
                ]))))
                return result

            self.target_aug_func = segmented_means

        if self.target_aug_func is not None:
            self.set_n_target_features(len(self.target_aug_func(torch.ones(self.vcsn_n_total_flattened))))
        else:
            self.set_n_target_features(self.vcsn_n_total_flattened)

    @classmethod
    def set_n_target_features(cls, num):
        if cls.n_target_features > 0 and cls.n_target_features != num:
            print(f'Warning: updating an existing n_target_features={cls.n_target_features} to {num}')
        cls.n_target_features = num

    @classmethod
    def vcsn2d(cls, flattened: Union[np.ndarray, torch.Tensor]) -> Union[np.ndarray, torch.Tensor]:
        # if len(flattened.shape) == 1:
        #     mask = cls.vcsn_mask
        # elif len(flattened.shape) == 2:
        #     mask = cls.vcsn_mask.reshape(
        #         flattened.shape[0], cls.vcsn_mask.shape[0], cls.vcsn_mask.shape[1])
        # else:
        #     raise ValueError(f'Invalid shape {flattened.shape}')
        assert len(flattened.shape) <= 2, f'Invalid shape {flattened.shape}'
        output_type = type(flattened)

        mask = torch.tensor(cls.vcsn_mask) if output_type is torch.Tensor else cls.vcsn_mask
        empty = torch.empty if output_type is torch.Tensor else np.empty
        output_shape = mask.shape if len(flattened.shape) == 1 else \
            (len(flattened), *mask.shape)

        img = empty(output_shape)
        if output_type is torch.Tensor:
            img = img.to(flattened.device)

        img[:] = float('NaN')
        if len(flattened.shape) == 1:
            img[mask] = flattened
        else:
            for i in range(len(flattened)):
                img[i, mask] = flattened[i]

        return img

    @staticmethod
    def month_of_sample(index, split='test'):
        first = TIME_FIRST_TEST if split == 'test' else TIME_FIRST_TRAIN
        last = TIME_LAST_TEST if split == 'test' else TIME_LAST_TRAIN
        if index >= 0:
            return first + index * np.timedelta64(1, 'M')
        else:
            return last + (index + 1) * np.timedelta64(1, 'M')

    @classmethod
    def flatten2category(cls, flattened: np.ndarray, index, split='test', normalized=False) -> dict:
        if normalized:
            flattened = flattened * cls.vcsn_flatten_std + cls.vcsn_flatten_mean

        # anomalies = np.empty(cls.vcsn_mask.shape)
        # anomalies[:] = float('NaN')
        # anomalies[cls.vcsn_mask] = flattened
        anomalies = cls.vcsn2d(flattened)

        month = cls.month_of_sample(index, split).astype(
            'datetime64[M]').astype(int) % 12

        return cls.anomalies2category(anomalies, month)

    @classmethod
    def anomalies2category(cls, anomalies: np.ndarray, month) -> dict:
        if callable(getattr(anomalies, 'numpy', None)):
            anomalies = anomalies.numpy()
        temperature = ClimateObservational.climato_mean[month].data + anomalies
        result = {}
        for region in ('NNI', 'ENI', 'WNI', 'NSI', 'WSI', 'ESI'):
            mask = ~np.isnan(cls.ds_regional_masks[region].data)
            mean = np.nanmean(temperature[mask])
            result[region] = 1 if mean < cls.df_rq[f'{region}_Q33'].iloc[month] else \
                2 if mean < cls.df_rq[f'{region}_Q66'].iloc[month] else 3
        return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gcm_root', type=str, default='ECMWF')
    parser.add_argument('--vcsn_root', type=str, default='VCSN')
    parser.add_argument('--split', type=str, default='train')
    parser.add_argument('--write_dataset', type=str, default='')
    args = parser.parse_args()

    write_dataset = args.write_dataset.split(',')
    if len(write_dataset) == 0:
        write_dataset = False
    ClimateObservational(args.gcm_root, args.vcsn_root, None, None, None,
            split=args.split, regenerate=True, write_dataset=write_dataset)
