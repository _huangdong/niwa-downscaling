import argparse
import random
from datetime import datetime
import sys
import os
from typing import Union

import torch
from torch.nn.functional import conv2d
import h5py
import numpy as np
from pathlib import Path

from glob import glob
import torch.utils.data as data
import xarray as xr
import pandas as pd
from dask.diagnostics import ProgressBar

LON_RANGE = (70, 294)
LAT_RANGE = (-70, 58)
TIME_BASIS = 'seasonal'
TIME_FIRST_TRAIN = np.datetime64('1993-04', 'M')
TIME_LAST_TRAIN = np.datetime64('2014-12', 'M')
TIME_FIRST_TEST = np.datetime64('2015-01', 'M')
TIME_LAST_TEST = np.datetime64('2016-12', 'M')
ECMWF_STEP = 3


class Climate(torch.utils.data.Dataset):
    vcsn_n_land_grids = 11_491
    vcsn_n_total_flattened = vcsn_n_land_grids
    ecmwf_ens_cache = {}
    vcsn_cache = {}
    vcsn_mean = None
    vcsn_std = None
    vcsn_mask = None
    vcsn_mask_da = None
    climato_mean = None
    df_rq = None
    ds_regional_masks = None
    ecmwf_mean = None
    ecmwf_std = None

    def __init__(self, gcm_root, vcsn_root, mask_root, img_transform, mask_transform,
                 split='train', image_size=72, n_masks=10000, regenerate=False,
                 sanity_check_area='', gt_vcsn=False, gt_gcm=False,
                 reuse_data_cache=True,
                 standardize=False,
                 vcsn_grad=False,
                 scalars=(),
                 gcm_3ms=False,
                 write_dataset=False):
        super(Climate, self).__init__()

        self.image_size = image_size
        self.n_masks = n_masks

        self.img_transform = img_transform
        self.transform = img_transform
        self.mask_transform = mask_transform
        self.subset = split
        self.gcm_root = str(gcm_root)
        self.vcsn_root = str(vcsn_root)
        self.regenerate = regenerate
        self.write_dataset = write_dataset
        self.sc_zone = sanity_check_area
        self.new_approach = True
        self.gt_vcsn = gt_vcsn
        self.gt_gcm = gt_gcm
        self.standardize = standardize
        self.vcsn_grad = vcsn_grad
        self.scalars = scalars
        self.gcm_3ms = gcm_3ms

        self.maskpath = mask_root

        self.reuse_data_cache = reuse_data_cache
        self._prepare_data()

    def __getitem__(self, index):
        if not self.gcm_3ms:
            image = torch.from_numpy(self.ecmwf_ens[index]).float().repeat(3, 1, 1)
        else:
            index += 1
            image = torch.stack((
                torch.from_numpy(self.ecmwf_ens[index - 1]).float(),
                torch.from_numpy(self.ecmwf_ens[index + 0]).float(),
                torch.from_numpy(self.ecmwf_ens[index + 1]).float(),
            ), dim=0)

        if len(self.scalars) > 0:
            selector = np.isin(np.arange(self.vcsn.shape[-1]), self.scalars)
            target = torch.from_numpy(self.vcsn[index, selector]).float()
        else:
            target = torch.from_numpy(self.vcsn[index]).float()

        return image, target

    def __len__(self):
        if self.gcm_3ms:
            return len(self.ecmwf_ens) - 2
        else:
            return len(self.ecmwf_ens)

    def _get_vcsn(self) -> xr.DataArray:
        with xr.open_mfdataset(glob(f'{self.vcsn_root}/TMEAN/*{TIME_BASIS}*.nc')) as ds:
            print(ds)
            vcsn = ds.rename({'Tmean_N': 't2m'})['t2m']
            type(self).vcsn_mask = ~np.isnan(ds.mask).compute().data
            type(self).vcsn_mask_da = ds.mask

        return vcsn

    def _get_ecmwf(self) -> xr.DataArray:
        ecmwf_files = [f'{self.gcm_root}/T2M/ECMWF_T2M_{TIME_BASIS}_anomalies_{year:04}_{month:02}.nc'
                       for year in range(1993, 2017) for month in range(1, 13)
                       ]
        with xr.open_mfdataset(ecmwf_files, concat_dim='time', parallel=True, combine='nested') as ds:
            ecmwf = ds['t2m']

        return ecmwf

    def _get_regional_climato(self) -> xr.DataArray:
        climato_file = os.path.join(os.path.dirname(__file__),
                                    '../dataset/climatology_average_Q33_Q66_Tmean.nc')
        with xr.open_dataset(climato_file) as ds:
            type(self).climato_mean = ds.Tmean_average.transpose(
                'month', 'lat', 'lon'
            )

        return self.climato_mean

    def _get_regional_quantities(self) -> None:
        quantities_file = os.path.join(os.path.dirname(__file__),
                                       '../dataset/Climatological_quantiles_3_cat_TMEAN.csv')
        with pd.read_csv(quantities_file, header=2, index_col=0) as df:
            df.columns = [f'{region}_{split}' for split in ('Q33', 'Q66')
                          for region in ('NNI', 'ENI', 'WNI', 'NSI', 'WSI', 'ESI')]
            type(self).df_rq = df

    def _get_regional_masks(self) -> None:
        nc_file = 'dataset/NZ_6_regions_mask.nc'
        with xr.open_dataset(nc_file) as ds:
            type(self).ds_regional_masks = ds

    def _process_data_regression(self):
        # Read the ECMWF data
        ecmwf = self._get_ecmwf()
        vcsn = self._get_vcsn()
        self._get_regional_climato()

        # Align the time between ECMWF and VCSN
        time_first = TIME_FIRST_TRAIN if self.subset == 'train' else TIME_FIRST_TEST
        time_last = TIME_LAST_TRAIN if self.subset == 'train' else TIME_LAST_TEST
        delta_step = np.timedelta64(ECMWF_STEP, 'M')
        delta_1month = np.timedelta64(1, 'M')
        vcsn = vcsn.where((time_first <= vcsn.time)
                          & ((time_last + delta_1month) > vcsn.time),
                          drop=True)
        ecmwf = ecmwf.where((time_first <= (ecmwf.time + delta_step.astype('timedelta64[ns]')))
                            & (time_last >= (ecmwf.time + delta_step.astype('timedelta64[ns]'))),
                            drop=True)

        # Aggregate in the ensembles and pick only one step.
        ecmwf_ens = ecmwf.sel({'step': ECMWF_STEP}).mean(dim='member')
        ecmwf_ens_std = ecmwf.sel({'step': ECMWF_STEP}).std(dim='member')

        # Rearrange the dimensions
        ecmwf_ens = ecmwf_ens.transpose('time', 'lat', 'lon')
        ecmwf_ens_std = ecmwf_ens_std.transpose('time', 'lat', 'lon')
        vcsn = vcsn.transpose('time', 'lat', 'lon')

        # Flatten VCSN
        vcsn_time_length = len(vcsn.time)
        n_flatten_feat = self.vcsn_n_land_grids
        vcsn_flatten = vcsn.data.reshape(vcsn_time_length, -1)
        if hasattr(vcsn_flatten, 'compute'):
            vcsn_flatten = vcsn_flatten.compute()
        vcsn_flatten = vcsn_flatten[~np.isnan(vcsn_flatten)]
        if vcsn_flatten.shape[0] != n_flatten_feat * vcsn_time_length:
            for i, instance in enumerate(vcsn.data.reshape(vcsn_time_length, -1)):
                print(f'instance={instance}')
                index = ~np.isnan(instance)
                print(f'index={index}')
                instance = instance[index]
                print(instance)
                l = len(instance)
                l = len(instance[~np.isnan(instance)])
                if l != n_flatten_feat:
                    raise ValueError(f'VCSN at time {vcsn[i].time} has incorrect '
                                     f'number of valid grid points: '
                                     f'expected {n_flatten_feat}, got {l}.')
        vcsn_flatten = vcsn_flatten.reshape(vcsn_time_length, n_flatten_feat)

        # If `vcsn_grad` specified, append the flattened 2D gradient to the attributes.
        if self.vcsn_grad:
            # img2d.shape: (time, channel=1, lat, lon)
            img2d = torch.from_numpy(vcsn.data.compute()).unsqueeze(1)
            dx = conv2d(img2d, torch.tensor(
                [[[[0, 0, 0], [-1, 0, 1], [0, 0, 0]]]], dtype=torch.float))
            dy = conv2d(img2d, torch.tensor(
                [[[[0, -1, 0], [0, 0, 0], [0, 1, 0]]]], dtype=torch.float))
            grad2d = torch.cat((dx.unsqueeze(-1), dy.unsqueeze(-1)), dim=-1)
            grad2d = grad2d.squeeze(1)  # remove the channel dimension
            grad2d_norm = grad2d.norm(dim=-1, p='fro')
            grad2d_norm_flatten = grad2d_norm[~torch.isnan(grad2d_norm)].view(
                vcsn_time_length, -1)
            print(f'grad2d_norm_flatten.shape={grad2d_norm_flatten.shape}')

            vcsn_flatten = np.hstack((vcsn_flatten, grad2d_norm_flatten.numpy()))
            print(f'vcsn_flatten.shape={vcsn_flatten.shape}')

            type(self).vcsn_n_total_flattened = vcsn_flatten.shape[1]

        ecmwf_ens = ecmwf_ens.data
        if hasattr(ecmwf_ens, 'compute'):
            ecmwf_ens = ecmwf_ens.compute()

        if self.subset == 'train':
            type(self).vcsn_mean = np.mean(vcsn_flatten, axis=0).reshape(1, vcsn_flatten.shape[1])
            type(self).vcsn_std = np.std(vcsn_flatten, axis=0).reshape(1, vcsn_flatten.shape[1])

            type(self).ecmwf_mean = np.mean(ecmwf_ens, axis=0)
            type(self).ecmwf_std = np.std(ecmwf_ens, axis=0)

        if self.standardize:
            print(f'Split {self.subset}: vcsn_mean={self.vcsn_mean}, vcsn_std={self.vcsn_std}')
            vcsn_flatten = (vcsn_flatten - self.vcsn_mean) / self.vcsn_std

            print(f'Split {self.subset}: ecmwf_mean={self.ecmwf_mean}, ecmwf_std={self.ecmwf_std}')
            ecmwf_ens = (ecmwf_ens - self.ecmwf_mean) / self.ecmwf_std

        self.ecmwf_ens = ecmwf_ens
        self.vcsn = vcsn_flatten

        type(self).ecmwf_ens_cache[self.subset] = ecmwf_ens
        type(self).vcsn_cache[self.subset] = vcsn_flatten

    def _prepare_data(self):
        """
        Data preparation.

        The first step of experiments:

        Using the combination of VCSN data for New Zealand and ERA5 for the rest in the world.

        :return:
        """

        if self.reuse_data_cache and self.ecmwf_ens_cache.get(self.subset) is not None \
                and self.vcsn_cache.get(self.subset) is not None:
            print('Reusing cached GCM and VCSN data.')
            self.ecmwf_ens = self.ecmwf_ens_cache[self.subset]
            self.vcsn = self.vcsn_cache[self.subset]
        else:
            self._process_data_regression()
            print('GCM and VCSN databases processed.')

    @classmethod
    def vcsn2d(cls, flattened: Union[np.ndarray, torch.Tensor]) -> Union[np.ndarray, torch.Tensor]:
        # if len(flattened.shape) == 1:
        #     mask = cls.vcsn_mask
        # elif len(flattened.shape) == 2:
        #     mask = cls.vcsn_mask.reshape(
        #         flattened.shape[0], cls.vcsn_mask.shape[0], cls.vcsn_mask.shape[1])
        # else:
        #     raise ValueError(f'Invalid shape {flattened.shape}')
        assert len(flattened.shape) <= 2, f'Invalid shape {flattened.shape}'
        output_type = type(flattened)

        mask = torch.tensor(cls.vcsn_mask) if output_type is torch.Tensor else cls.vcsn_mask
        empty = torch.empty if output_type is torch.Tensor else np.empty
        output_shape = mask.shape if len(flattened.shape) == 1 else \
            (len(flattened), *mask.shape)

        img = empty(output_shape)
        if output_type is torch.Tensor:
            img = img.to(flattened.device)

        img[:] = float('NaN')
        if len(flattened.shape) == 1:
            img[mask] = flattened
        else:
            for i in range(len(flattened)):
                img[i, mask] = flattened[i]

        return img

    @staticmethod
    def month_of_sample(index, split='val'):
        first = TIME_FIRST_TEST if split == 'val' else TIME_FIRST_TRAIN
        last = TIME_LAST_TEST if split == 'val' else TIME_LAST_TRAIN
        if index >= 0:
            return first + index * np.timedelta64(1, 'M')
        else:
            return last + (index + 1) * np.timedelta64(1, 'M')

    @classmethod
    def flatten2category(cls, flattened: np.ndarray, index, split='val') -> dict:

        # anomalies = np.empty(cls.vcsn_mask.shape)
        # anomalies[:] = float('NaN')
        # anomalies[cls.vcsn_mask] = flattened
        anomalies = cls.vcsn2d(flattened)

        month = cls.month_of_sample(index, split).astype(
            'datetime64[M]').astype(int) % 12

        return cls.anomalies2category(anomalies, month)

    @classmethod
    def anomalies2category(cls, anomalies: np.ndarray, month) -> dict:
        temperature = anomalies + Climate.climato_mean[month]
        result = {}
        for region in ('NNI', 'ENI', 'WNI', 'NSI', 'WSI', 'ESI'):
            mask = ~np.isnan(cls.ds_regional_masks[region].data)
            mean = temperature[mask].mean()
            result[region] = 1 if mean < cls.df_rq[f'{region}_Q33'] else \
                2 if mean < cls.df_rq[f'{region}_Q66'] else 3
        return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gcm_root', type=str, default='ECMWF')
    parser.add_argument('--vcsn_root', type=str, default='VCSN')
    parser.add_argument('--split', type=str, default='train')
    parser.add_argument('--write_dataset', type=str, default='')
    args = parser.parse_args()

    write_dataset = args.write_dataset.split(',')
    if len(write_dataset) == 0:
        write_dataset = False
    Climate(args.gcm_root, args.vcsn_root, None, None, None,
            split=args.split, regenerate=True, write_dataset=write_dataset)
