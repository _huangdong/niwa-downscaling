#!/bin/bash
RUN_DIR=$(pwd)
TOP_DIR=$RUN_DIR/../..
base_cmd() {
    cd $TOP_DIR
    date
    python main.py \
    --gcm_root\
    /Scratch/dh146/ECMWF\
    --vcsn_root\
    /Scratch/dh146/VCSN\
    --regression_vcsn\
    --lr\
    0.001\
    --lr_interval\
    40\
    --arch\
    efficientnet-b0\
    --batch_size\
    40\
    --weight_decay\
    1e-4\
    --epochs\
    90\
    --save_checkpoint\
    --smooth_type\
    grad2d\
    --smooth_lambda\
    1e-15\
    --sst_rain\
    $*
    cd $RUN_DIR
}

test_name=sst_rain_run1
base_cmd --tb_tag $test_name --sst_rain | tee $test_name.log
mv $TOP_DIR/model.pth $test_name.pth

