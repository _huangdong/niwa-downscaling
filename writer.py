import os
from pathlib import Path

import torch
from torch.utils.tensorboard import SummaryWriter
import pandas as pd
import xarray as xr


class MySummaryWriter(SummaryWriter):
    def __init__(self, log_dir=None, comment='', purge_step=None, max_queue=10,
                 flush_secs=120, filename_suffix=''):
        super().__init__(log_dir, comment, purge_step, max_queue, flush_secs, filename_suffix)

        dir_levels = str(self.log_dir).rstrip('/').split('/')
        self.csv_dir = '/'.join(dir_levels[:-1]) + '-csv'
        self.scalar_csv_file = os.path.join(self.csv_dir, dir_levels[-1] + '.scalar.csv')
        self.text_csv_file = os.path.join(self.csv_dir, dir_levels[-1] + '.text.csv')
        print(f'scalar csv is {self.scalar_csv_file}, text csv is {self.text_csv_file}')
        try:
            os.mkdir(self.csv_dir)
        except FileExistsError:
            pass
        self.scalars = []
        self.texts = []

    def add_tensor(self, tag, tensor_value, dims=None, global_step=None, walltime=None):
        self.tensors[tag] = {'data': tensor_value, 'dims': dims,
                             'global_step': global_step, 'walltime': walltime}

        if dims:
            ds = xr.Dataset({tag: (dims, tensor_value.numpy())},
                            {'step': global_step})

    def add_vector(self, tag, vector_value, **kwargs):
        for i, scalar in enumerate(vector_value):
            self.add_scalar(f'{tag}_{i}', scalar, write_tb=False, **kwargs)

    def add_scalar(self, tag, scalar_value, global_step=None, walltime=None,
                   write_tb=True, write_csv=True):
        if write_tb:
            super().add_scalar(tag, scalar_value, global_step, walltime)

        if write_csv:
            if type(scalar_value) is torch.Tensor:
                scalar_value = scalar_value.item()
            self.scalars.append({'tag': tag, 'value': scalar_value, 'step': global_step, 'walltime': walltime})

    def add_text(self, tag, text_string, global_step=None, walltime=None):
        super().add_text(tag, text_string, global_step, walltime)

        self.texts.append({'tag': tag, 'text': text_string, 'step': global_step, 'walltime': walltime})

    def flush(self):
        super().flush()
        pd.DataFrame(self.scalars).to_csv(self.scalar_csv_file)
        pd.DataFrame(self.texts).to_csv(self.text_csv_file)
