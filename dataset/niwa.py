import os
import logging

import pandas as pd
import numpy as np
from PIL import Image

from .versioned_directory import VersionedDirectory
from .versioned_dataset import VersionedDataset


class NiwaEquiRectLoader:
    n_channels: int = 3

    def __init__(self, width, height):
        self.width = width
        self.height = height

    def __call__(self, sample):
        try:
            channels = np.random.choice(len(sample), self.n_channels, replace=False)
        except ValueError as e:
            if len(sample) < self.n_channels:
                channels = np.random.choice(len(sample), self.n_channels, replace=True)
            else:
                raise e
        sample = np.array(sample)[channels]
        sample = sample.reshape(
            (self.n_channels, self.width, self.height))
        try:
            ret = Image.fromarray(sample, mode='RGB')
        except TypeError as e:
            raise e

        return ret


class NiwaDS(VersionedDataset):
    version = 1
    versioned_directory = type('', (VersionedDirectory,), {
        'src_url': 'https://weka9.resnet.cms.waikato.ac.nz/datasets/niwa/',
        'archive_name': 'niwa'
    })

    df_train: pd.DataFrame
    df_test: pd.DataFrame

    rain_csv = r'GCMs_std_and_targets_cat3_and_anomalies_RAIN_{}_set.csv'
    tmean_csv = r'GCMs_std_and_targets_cat3_and_anomalies_TMEAN_{}_set.csv'

    n_grid_lat = 53
    n_grid_lng = 93

    GCM_LIST = ('ECMWF', 'UKMO', 'METEO_FRANCE', 'DWD', 'CMCC', 'NCEP_CFSv2',
                           'CanCM4i', 'GEM_NEMO', 'NASA_GEOSS2S', 'CanSIPSv2', 'JMA')

    def __init__(self, root, samples, transform=None, target_transform=None,
                 loader=None, **kwargs):
        # super().__init__(root, samples, transform, target_transform, loader, **kwargs)
        super().__init__(root, samples, transform=transform)
        self.transform = transform
        self.target_transform = target_transform
        self.samples = samples

        if loader is None:
            self.loader = NiwaEquiRectLoader(self.n_grid_lat, self.n_grid_lng)

    @staticmethod
    def _adjust_cls_label(index):
        return index - 1

    @classmethod
    def _make_samples(cls, root, **kwargs):
        climate = kwargs.get('climate', 'RAIN')
        assert climate in ('RAIN', 'TMEAN')

        region = kwargs.get('region', 'NNI')
        assert region in ('ENI', 'ESI', 'NNI', 'NSI', 'WNI', 'WSI')

        gcm = kwargs.get('gcm', 'JMA')
        if ',' in gcm:
            gcm = list(filter(None, gcm.split(',')))
            assert set(cls.GCM_LIST).issuperset(gcm)
        elif gcm.lower() == 'all':
            gcm = cls.GCM_LIST
        else:
            assert gcm in cls.GCM_LIST
            gcm = [gcm]

        if climate == 'RAIN':
            csv = cls.rain_csv
        else:
            csv = cls.tmean_csv

        print(f'climate={climate}, gcm={gcm}, region={region}')

        df_train = pd.read_csv(os.path.join(root, 'CSV/TRAIN', csv.format('training')))
        df_test = pd.read_csv(os.path.join(root, 'CSV/TEST', csv.format('test')))

        df_train.columns = ['Month', *df_train.columns[1:]]
        df_test.columns = ['Month', *df_test.columns[1:]]

        assert df_train.columns.equals(df_test.columns)
        columns = df_train.columns
        selected_columns = columns[1:columns.get_loc('GCM')]
        # selected_columns = selected_columns.insert(len(selected_columns), f'{region}_cat3_categories')

        # train_samples = df_train[df_train.GCM == gcm].loc[:, selected_columns]
        # test_samples = df_test[df_test.GCM == gcm].loc[:, selected_columns]
        #
        # train_samples = train_samples.apply(lambda row: [row[:-1].tolist(), row[-1]], axis=1).values.tolist()
        # test_samples = test_samples.apply(lambda row: [row[:-1].tolist(), row[-1]], axis=1).values.tolist()

        def collect_into_months(df):
            months = df.Month.unique()
            samples = []
            for month in months:
                selected_rows = df[df.Month == month]
                data = []
                for i in range(len(selected_rows)):
                    if selected_rows.iloc[i, :]['GCM'] in gcm:
                        data.append(selected_rows.iloc[i, :].loc[selected_columns].values.tolist())

                if len(data) == 0:
                    continue

                class_label = NiwaDS._adjust_cls_label(
                    selected_rows.iloc[0, :][f'{region}_cat3_categories'])
                samples.append((data, class_label))

            return samples

        train_samples = collect_into_months(df_train)
        test_samples = collect_into_months(df_test)

        cls.df_train = df_train
        cls.df_test = df_test

        return train_samples, test_samples, None
